package com.example.mymap.maps_activity.vm

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.*
import com.example.data.models.VehicleDTO
import com.example.data.models.VehiclesRequest
import com.example.data.remote.ResultHandler
import com.example.data.repositories.VehiclesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


/**
 * Created by Javier Sarmiento
 */

class MapViewModel(private val repository: VehiclesRepository) : ViewModel() {

    val vehiclesList: LiveData<List<VehicleDTO>> = repository.mVehiclesList

    fun fetchVehiclesList(){
        viewModelScope.launch (Dispatchers.IO) {
            val vehicesRequest = VehiclesRequest(38.711046,38.739429,-9.160096,-9.137115)
            when (val result = repository.getVehiclesAndSave(vehicesRequest)){
                is ResultHandler.Success -> {
                    Log.d(TAG, "fetchVehiclesList: OK")
                }
            }
        }
    }

}