package com.example.mymap.maps_activity.ui

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.data.models.CompanyZoneEnum
import com.example.data.models.VehicleDTO
import com.example.mymap.R
import com.example.mymap.databinding.FragmentMapsBinding
import com.example.mymap.general.BaseFragment
import com.example.mymap.general.Constants
import com.example.mymap.maps_activity.vm.MapViewModel

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

/**
 * Created by Javier Sarmiento
 */

class MapFragment : BaseFragment(), OnMapReadyCallback {

    private val presenter: MapViewModel by sharedViewModel()
    private var _binding: FragmentMapsBinding? = null
    private val binding get() = _binding!!
    var vehiclesList: List<VehicleDTO>? = null
    private lateinit var mMap: GoogleMap

    override fun loadObservers() {
        Log.d(TAG, "loadObservers: A VER")
        presenter.vehiclesList.observe(viewLifecycleOwner, {
            vehiclesList = it
            fillMap()
        })
        presenter.fetchVehiclesList()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapsBinding.inflate(inflater, container, false)
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        return binding.root
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        // Add a marker in Sydney and move the camera
        val startPoint = LatLng(38.711046, -9.160096)
        mMap.addMarker(MarkerOptions().position(startPoint).title("Start point"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startPoint, 15.0f))
    }

    private fun fillMap() {
        for (vehicle in vehiclesList.orEmpty()) {
            val latlng = LatLng(vehicle.y, vehicle.x)
            var bitmapDescriptor: BitmapDescriptor =
                BitmapDescriptorFactory.fromResource(R.drawable.ic_bike)

            val vehicleType: String = if (!vehicle.resourceType.isNullOrEmpty() && vehicle.resourceType == Constants.RESOURCE_TYPE_MOTORBIKE) {
                Constants.RESOURCE_TYPE_MOTORBIKE
            } else if (vehicle.bikesAvailable != null) {
                Constants.RESOURCE_TYPE_BIKE
            } else {
                Constants.RESOURCE_TYPE_DEFAULT
            }

            when (Pair(vehicle.companyZoneId, vehicleType)) {
                Pair(CompanyZoneEnum.COMPANY_ZONE_473.id, Constants.RESOURCE_TYPE_MOTORBIKE) ->
                    bitmapDescriptor =
                        BitmapDescriptorFactory.fromResource(R.drawable.ic_motorbike_round)
                Pair(CompanyZoneEnum.COMPANY_ZONE_473.id, Constants.RESOURCE_TYPE_BIKE) ->
                    bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.ic_bike)
                Pair(CompanyZoneEnum.COMPANY_ZONE_412.id, Constants.RESOURCE_TYPE_BIKE) ->
                    bitmapDescriptor =
                        BitmapDescriptorFactory.fromResource(R.drawable.ic_bike_yellow)
                Pair(CompanyZoneEnum.COMPANY_ZONE_412.id, Constants.RESOURCE_TYPE_BIKE) ->
                    bitmapDescriptor =
                        BitmapDescriptorFactory.fromResource(R.drawable.ic_bike_yellow)
            }

            mMap.addMarker(
                MarkerOptions().position(latlng).title(vehicle.name).icon(bitmapDescriptor)
            )

        }
    }

}