package com.example.mymap.di

import com.example.mymap.maps_activity.vm.MapViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Javier Sarmiento
 */

val uiModule = module {
    viewModel { MapViewModel(get()) }
}