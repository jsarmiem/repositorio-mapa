package com.example.mymap.general

import androidx.appcompat.app.AppCompatActivity
/**
 * Created by Javier Sarmiento
 */
abstract class BaseActivity: AppCompatActivity()