package com.example.mymap.general

import android.app.Application
import com.example.data.di.dataModule
import com.example.mymap.di.uiModule

import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App) //Context
            modules(listOf(dataModule, uiModule)) //Modules
        }
    }
}