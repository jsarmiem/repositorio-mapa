package com.example.mymap.general


/**
 * Created by Javier Sarmiento
 */
object Constants {
    const val RESOURCE_TYPE_MOTORBIKE = "MOPED"
    const val RESOURCE_TYPE_BIKE = "BIKE"
    const val RESOURCE_TYPE_DEFAULT = "DEFAULT"
}