package com.example.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.general.Constants.ABSCISSA
import com.example.data.general.Constants.ORDINATE
import com.example.data.general.Constants.TABLE_VEHICLES
import com.example.data.models.VehicleDTO

/**
 * Created by Javier Sarmiento
 */

@Dao
interface VehicleDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(vehicles: List<VehicleDTO>)

    @Query("SELECT * FROM $TABLE_VEHICLES  WHERE " +
            "($ABSCISSA BETWEEN :startX AND :endX) AND " +
            "($ORDINATE BETWEEN :startY AND :endY)")
    fun loadVehiclesByRange(startX: Double, endX: Double, startY: Double, endY: Double): LiveData<List<VehicleDTO>>

}