package com.example.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.data.general.Constants.DATABASE_NAME
import com.example.data.models.VehicleDTO

/**
 * Created by Javier Sarmiento
 */
@Database(entities = [VehicleDTO::class], version = 1)
abstract class VehiclesDatabase: RoomDatabase() {
    abstract fun vehicleDao(): VehicleDAO

    companion object{
        @Volatile
        private var INSTANCE: VehiclesDatabase? = null

        fun getInstance(context: Context): VehiclesDatabase =
                INSTANCE ?: synchronized(this){
                    INSTANCE ?: buildDatabase(context.applicationContext).also {
                        INSTANCE = it
                    }
                }

        private fun buildDatabase(appContext: Context): VehiclesDatabase{
            return Room.databaseBuilder(appContext, VehiclesDatabase::class.java, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build()
        }
    }
}