package com.example.data.di

import com.example.data.di.providers.*
import org.koin.dsl.module

/**
 * Created by Javier Sarmiento
 */

val dataModule = module {
    single { provideOkHttpClient() }
    single { provideGson() }
    single { provideRetrofit( get(), get()) }
    single { provideMapsApi( get() ) }
    single { provideBankDatabase( get() ) }
    single { provideVehicleRepository( get(), get() ) }
}