package com.example.data.di.providers

import android.app.Application
import com.example.data.local.VehiclesDatabase
import com.example.data.remote.IAPI
import com.example.data.repositories.VehiclesRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 * Created by Javier Sarmiento
 */


fun provideOkHttpClient(): OkHttpClient{
    val client = OkHttpClient().newBuilder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
    return client.build()
}

fun provideGson(): Gson {
    return GsonBuilder()
            .setLenient()
            .serializeNulls()
            .create()
}

fun provideRetrofit(httpClient: OkHttpClient, gson: Gson): Retrofit = Retrofit.Builder()
        .client(httpClient)
        .baseUrl(IAPI.BASE_DEV_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
fun provideBankDatabase(application: Application): VehiclesDatabase {
    return VehiclesDatabase.getInstance(application)
}

fun provideMapsApi (retrofit: Retrofit): IAPI = retrofit
    .create(IAPI::class.java)
fun provideVehicleRepository(retrofit: IAPI, vehiclesDB: VehiclesDatabase): VehiclesRepository = VehiclesRepository(retrofit, vehiclesDB)

