package com.example.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.data.general.Constants

/**
 * Created by Javier Sarmiento
 */

@Entity(tableName = Constants.TABLE_VEHICLES)
data class VehicleDTO(
    @PrimaryKey val id : String,
    val name : String,
    val x : Double,
    val y : Double,
    val licencePlate : String?,
    val range : Int?,
    val batteryLevel : Int?,
    val helmets : Int?,
    val model : String?,
    val resourceImageId : String?,
    val realTimeData : Boolean?,
    val resourceType : String?,
    val companyZoneId : Int,
    val bikesAvailable : Int?
)
