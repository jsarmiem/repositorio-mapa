package com.example.data.models

/**
 * Created by Javier Sarmiento
 */

data class VehiclesRequest(
    val xStart : Double,
    val xEnd : Double,
    val yStart : Double,
    val yEnd : Double
)
