package com.example.data.models

/**
 * Created by Javier Sarmiento
 */
enum class CompanyZoneEnum(val id: Int){
    COMPANY_ZONE_473( 473),
    COMPANY_ZONE_412(412),
    UNKNOWN(-1);

    companion object{
        @JvmStatic
        fun getValueFromId(id: Int): CompanyZoneEnum? = values().find { it.id == id }
    }
}