package com.example.data.remote

import com.example.data.models.VehicleDTO
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by Javier Sarmiento
 */
interface IAPI {

    companion object {
        const val BASE_DEV_URL = "https://apidev.meep.me/tripplan/api/v1/routers/"
        const val LISBOA_VEHICLES = "lisboa/resources"
    }

    @GET(BASE_DEV_URL + LISBOA_VEHICLES)
    suspend fun getVehicles(@Query("lowerLeftLatLon") lowerLeftLatLon: String,
                            @Query("upperRightLatLon") upperRightLatLon: String) : Response<List<VehicleDTO>>


}