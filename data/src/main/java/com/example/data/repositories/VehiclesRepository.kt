package com.example.data.repositories

import androidx.lifecycle.MutableLiveData
import com.example.data.general.BaseRepository
import com.example.data.local.VehiclesDatabase
import com.example.data.models.VehicleDTO
import com.example.data.models.VehiclesRequest
import com.example.data.remote.IAPI
import com.example.data.remote.ResultHandler
import com.example.data.utils.VehicleUtils

/**
 * Created by Javier Sarmiento
 */
class VehiclesRepository(private val api: IAPI, private val vehiclesDB: VehiclesDatabase) :
    BaseRepository() {

    var mVehiclesList: MutableLiveData<List<VehicleDTO>> = MutableLiveData()


    suspend fun getVehiclesAndSave(vehiclesRequest: VehiclesRequest): ResultHandler<String> {
        //Call to API and save in Room

        when (val result = safeApiCall(call = {
            api.getVehicles(
                VehicleUtils.coordinateToString(vehiclesRequest.xStart, vehiclesRequest.yStart),
                VehicleUtils.coordinateToString(vehiclesRequest.xEnd, vehiclesRequest.yEnd)
            )})) {
            is ResultHandler.Success -> {
                //Sort the list
                result.data.let {

                    val vehicleResponse: List<VehicleDTO> = result.data
                    mVehiclesList.postValue(vehicleResponse)
                    //Save data in Room
                    //vehiclesDB.vehicleDao().save(vehicleResponse)
                }
                //It is not necessary to return nothing, magic is done with liveData in Room
                return ResultHandler.Success("Successful update")
            }
            is ResultHandler.GenericError -> return result
            is ResultHandler.HttpError -> return result
            is ResultHandler.NetworkError -> return result

        }
        return ResultHandler.GenericError("???")
    }

}