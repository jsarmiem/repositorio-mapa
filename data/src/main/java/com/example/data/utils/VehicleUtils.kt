package com.example.data.utils

/**
 * Created by Javier Sarmiento
 */

object VehicleUtils {
    fun coordinateToString(abscissa: Double, ordinate: Double): String {
        return "$abscissa,$ordinate"
    }
}

