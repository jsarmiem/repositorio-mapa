package com.example.data.general

/**
 * Created by Javier Sarmiento
 */
object Constants {
    const val DATABASE_NAME = "vehicles_db"
    const val TABLE_VEHICLES = "vehicles"

    const val ABSCISSA  = "x"
    const val ORDINATE  = "y"

}